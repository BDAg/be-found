# Be Found



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/BDAg/be-found.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/BDAg/be-found/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Name
BeFound

## Description
O BeFound se trata de um website desenvolvido como Projeto Integrador do 1° Termo do Curso de BigData no Agronegócio da Fatec Shunji Nishimura em Pompéia-SP. O objetivo inicial era desenvolver um web-crawler que capturasse os dados de pessoas desaparecidas e os armazenasse em um website de interface e acesso mais simples. Para tal, desenvolvemos o Front-End em Angular, e o Back-End em NodeJS, sendo ambos frameworks JavaScript, cada qual com uma área de atuação. O web-crawler foi desenvolvido em Python através da biblioteca Selenium, que capturou Nome, Data de Nascimento e Cidade Natal de todos os desaparecidos presentes no site da Secretaria de Segurança do Estado de São Paulo e os exportou para um arquivo CSV. Para que os dados fossem mostrados no website, criamos um banco de dados e um arquivo de import.js, também em NodeJS que realizou o upload dos dados para um banco de dados MySQL que foi conectado com o Front através da API.

## Support
Para suporte, entre em contato com os desenvolvedores nos e-mails a seguir:
ferdevferrari@gmail.com (Fernanda Ferrari)
danieldodesign2005@gmail.com (Daniel Lucas)
jacquelinenakagawa@gmail.com (Jacqueline Nakagawa)

## Roadmap
As próximas ondas definidas no MVP, ou seja, as futuras implementações previstas são as telas de perfil do usuário, uma tela específica para o desaparecido contendo todas as suas informações, um chat para contato com o usuário cadastrante do desaparecido e notificações.

## Contributing
Estamos abertos para sugestões e ajuda caso algém se interesse pelo desenvolvimento do projeto.

O script database deve ser colado no MySQL Workbench, para funcionamento do Back-end é necessário cloná-lo e instalar as dependências através do comando 
 -- npm install
 para fazer funcionar, digite no terminal
 -- node index.js
 ou
 -- nodemon index.js

 Para execução do front-end, é preciso clonar o repositório e assim como no back, instalar as dependências através do 
 -- npm install
 o próximo passo é iniciar com
 -- ng serve
 e assim abrir a página no navegador através da URL "http://localhost:4200"

 para instalação do Selenium basta digitar o comando 
 <pip install selenium> 
 no terminal do computador ou pelo software de desenvolvimento(como o VSCode), logo após é necessário instalar o driver controlador do seu navegador; para isso, é necessário encontrar a versão do seu navegador Chrome ou Firefox e baixar o driver através do site 
 <https://chromedriver.chromium.org/downloads> para o Google Chrome, 
 ou <https://github.com/mozilla/geckodriver/releases> para o Mozilla Firefox, após baixado esse arquivo, é necessário descompactá - lo na pasta onde fica armazenado Python no seu computador e pronto, você já pode utilizar o Selenium.

## Authors and acknowledgment
Daniel Lucas Ramos Barbante LinkedIn: (https://www.linkedin.com/in/daniel-lucas-665bb114b/) GitHub: (https://github.com/DanielLucas2305) -> Desenvolvimento do WebCrawler, Edição do Pitch

Fernanda Ferreira Ferrari LinkedIn: (https://www.linkedin.com/in/fernanda-ferrari-85428b274/) GitHub: (https://github.com/fferrar1) -> Gestão, MVP, Banco de Dados e API

Jacqueline Akina Nakagawa LinkedIn: (https://www.linkedin.com/in/jacqueline-nakagawa-9b9131a6/) GitHub: (https://github.com/JacqueNakagawa) -> Desenvolvimento Front-end, Pitch, Roteiro e Relatório Final


## Project status
O projeto não está 100% concluído, devido ao pouco tempo que tivemos para o desenvolvimento, porém são poucas as alterações necessárias para seu funcionamento perfeito.